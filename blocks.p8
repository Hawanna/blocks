pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
-- global values
blockwidth = 8
loopsize = 121
gravity = 4

-- display information
ui = {
 x = 0,
 y = 4,
	draw = function(me)
		--score
		print(player.score,me.x+12,me.y,10)
		spr(48,me.x+3,me.y-1,1,1)
		--health
		--background bar
		local bar_w = 40
		local bar_h = 8
		local bar_x = me.x+82
		local bar_y = me.y
		rectfill(bar_x+1,bar_y+1,bar_x+bar_w-1,bar_y+bar_h-1,6)
		--value bar
		rectfill(bar_x+1,bar_y+1,bar_x+((player.maxlife/100) * player.life/100 * (bar_w+1))-2,bar_y+bar_h-1,8)
		--overlay
		line(bar_x+1,bar_y,bar_x+bar_w-1,bar_y,0)
		line(bar_x+1,bar_y+bar_h,bar_x+bar_w-1,bar_y+bar_h,0)
		line(bar_x,bar_y+1,bar_x,bar_y+bar_h-1,0)
		line(bar_x+bar_w,bar_y+1,bar_x+bar_w,bar_y+bar_h-1,0)
	end,
	update = function(me)
	end
}

-- global player object
player = {
	x = 64,
	y = 8,
	left = false,
	speed = 2,
	j_frames = 12,
	jumping = false,
	falling = false,
	j_strength = 6,
 j_frame = 0,
	dimension = 8,
	idle = {0,1,2},
	walking = {16,17,18,19},
	dropping = {32},
	ascending = {33},
	sprites = {},
	frame = 1,
	tick = 0,
	step = 4,
	maxlife = 100,
	life = 100,
	score = 0,
	update = function(me)
	 --check for input and movement
	 --falling
  local current = blocks:get_block_within(me.x,me.y)
  local down 
  if(not me.left) then
  	down = blocks:get_block_within(me.x,me.y+blockwidth+1)       
  else
   down = blocks:get_block_within(me.x+me.dimension/2,me.y+blockwidth+1)       
  end
		local left = blocks:get_block_within(me.x-1,me.y)       
		local right = blocks:get_block_within(me.x+blockwidth,me.y)       
		local up = blocks:get_block_within(me.x,me.y-1)

		if(down and down.btype ~= "air" and not me.jumping) then
			me.y = down.y - blockwidth
			me.falling = true
		else
		 if(down and down.btype == "ground") then
		 	me.falling = false	
		 end 
		 if(me.jumping and up and up.btype == "air") then
		 	if(me.j_frame == me.j_frames or blocks:get_block_within(me.x,me.y-me.j_strength).btype == "ground") then
		 		me.jumping = false
		 		me.j_frame = 0
		 		me.falling = true
		 	else
		 		local jump_calc = me.j_strength -  (me.j_frames / 100) * me.j_frame * me.j_strength
		 		me.y -= jump_calc
		 		me.j_frame+=1
		 	end
		 else
				me.y += gravity
			end
		end
		--grounded?
		if(down and down.btype == "ground" or me.jumping) then
			me.falling = false
		end
	 --moving
	 if(btn(2,0) or btn(5,0)) then
	  if(not me.jumping and not me.falling and up and up.btype == "air" and down.btype == "ground") then
 			me.jumping = true 
			end	 	
	 end
	 if(btn(0,0) and left and left.btype == "air") then
	 	me.sprites = me.walking
	 	me.x -= me.speed
	 	me.left = true
	 else
 	 if(btn(1,0) and right and right.btype == "air") then --fix 126 ?
 	  me.sprites = me.walking
 	  me.x += me.speed
 	  me.left = false
 	 else
 	 	me.sprites = me.idle
 	 end
	 end
	 if(me.falling) then
			me.sprites = me.dropping
		end
		if(me.jumping) then
			me.sprites = me.ascending
		end
		animate(me)
	end,
	draw = function(me) 
		palt(7,true)
		palt(0,false)
		spr(me.sprites[me.frame],me.x,me.y,1,1,me.left)
		palt()
	end
}


golds = {
 create_gold = function(me,nx,ny)
 	local g = {
  	sprites = {48,49,50,51,52,53,54,56,57},
  	frame = 1,
  	tick = 0,
  	step = 4,
  	value = 10,
  	x = nx,
  	y = ny,
  	dimension = 4,
  	update = function(me)
    animate(me)
    --check for collision with player
    if(sprite_collides(me,player)) then
    	player.score += me.value
    	golds:remove_gold(me)
    end
  	end,
  	draw = function(me)
   	spr(me.sprites[me.frame],me.x,me.y,1,1,me.left)
  	end,
		}
		me:add_gold(g)
 end,
 private_golds = {},
 add_gold = function(me,g)
  add(me.private_golds,g)
 end,
 remove_gold = function(me,g)
		del(me.private_golds,g)
 end,
	update = function(me)
	 foreach(me.private_golds,function(g)
  	g:update()
  end) 
	end,
	draw = function(me)
	 foreach(me.private_golds,function(g)
  	g:draw()
  end)
	end
}
-- global blocks array
blocks = {
	update = function(me)
	 for y=1,loopsize,blockwidth do
 	 for x=1,loopsize,blockwidth do
 	 	me.private_blocks[y][x]:update()
 	 end
	 end
	end,
	draw = function(me)
		for y=1,loopsize,blockwidth do
		for x=1,loopsize,blockwidth do
		 me.private_blocks[y][x]:draw()
		end
		end
		for y=1,loopsize,blockwidth do
		for x=1,loopsize,blockwidth do
		 me.private_blocks[y][x]:draw_outline()
		end
		end
	end,
	add_block = function(me,new_block)
		me.private_blocks[new_block.y+1][new_block.x+1]=new_block
	end,
	private_blocks = {},
	get_block_at = function(me,y,x)
		--return block at array position
		if(me.private_blocks[y+1] and me.private_blocks[y+1][x+1]) then
			return me.private_blocks[y+1][x+1]
		end
	end,
	get_block_within = function(me,x,y)
		--get block at coordinate
		--determine next lower 8. step
	 local by = flr((y)/blockwidth) * blockwidth
	 local bx = flr((x)/blockwidth) * blockwidth
		--return block at array pos
		return me:get_block_at(by,bx)	 
	end
}
--build a block
function create_block()
 	return {
 	x = 0,
 	y = 0,
 	c = 12,
 	btype = "air",
 	draw = function(me)
 		--switch replacement
			rectfill(me.x,me.y,me.x+blockwidth,me.y+blockwidth,me.c)
 	end,
	draw_outline = function(me)
		--check for other type around
		--then draw border if required
		local cur_block = blocks:get_block_within(me.x+blockwidth/2,me.y+blockwidth/2) 
		local up = blocks:get_block_at(cur_block.y-blockwidth,cur_block.x)
		local down = blocks:get_block_at(cur_block.y+blockwidth,cur_block.x)
		local left = blocks:get_block_at(cur_block.y,cur_block.x-blockwidth)
		local right = blocks:get_block_at(cur_block.y,cur_block.x+blockwidth)
		if(me.btype == "ground") then
			if(up and up.btype == "air") then
				line(me.x+1,me.y,me.x+blockwidth-1,me.y,0)
				pset(me.x+blockwidth,me.y,12)
				pset(me.x,me.y+1,0)
				pset(me.x+blockwidth,me.y+1,0)
			end
			if(down and down.btype == "air") then
				line(me.x+1,me.y+blockwidth,me.x+blockwidth-1,me.y+blockwidth,0)
				pset(me.x,me.y+blockwidth-1,0)
				pset(me.x+blockwidth,me.y+blockwidth-1,0)
				pset(me.x,me.y+blockwidth,12)
			end
			if(left and left.btype == "air") then
				line(me.x,me.y,me.x,me.y+blockwidth-1,0)
				pset(me.x,me.y,12)
				pset(me.x+1,me.y,0)
				pset(me.x+1,me.y+blockwidth,0)
			end
			if(right and right.btype == "air") then
				line(me.x+blockwidth,me.y+1,me.x+blockwidth,me.y+blockwidth-1,0)
				pset(me.x+blockwidth,me.y+blockwidth,12)
				pset(me.x+blockwidth-1,me.y,0)
				pset(me.x+blockwidth-1,me.y+blockwidth,0)
			end
		end
	end,
 	update = function(me)
 		if(me.btype == "air") then
 	 	me.c = 12
			end
			if(me.btype == "ground") then
				me.c = 4
			end
 	end
 }
end


function _init()
	--build scene
	--build_level()
	build_debug()
end

function build_debug() 
	-- init all blocks
	for y=1,loopsize,blockwidth do
		blocks.private_blocks[y] = {}
		for x=1,loopsize,blockwidth do
			local block = create_block()
			block.x = x-1
			block.y = y-1
			--is ground?
 		if(block.y > loopsize*0.7) then
				block.btype = "ground"
				if(block.x >= 80 and block.x < 113 and block.y == 88) then
					block.btype = "air"
				end
			end
			blocks:add_block(block)
		end		
	end
	--some platforms
	blocks:get_block_within(64,64).btype = "ground"
	blocks:get_block_within(72,64).btype = "ground"
	blocks:get_block_within(56,64).btype = "ground"
	blocks:get_block_within(16,48).btype = "ground"
	blocks:get_block_within(8,48).btype = "ground"
	blocks:get_block_within(8,56).btype = "ground"
	blocks:get_block_within(8,64).btype = "ground"
	blocks:get_block_within(8,72).btype = "ground"
	blocks:get_block_within(16,72).btype = "ground"
	blocks:get_block_within(24,72).btype = "ground"
	blocks:get_block_within(24,48).btype = "ground"
	blocks:get_block_within(32,48).btype = "ground"
	
	--add some debug gold
	golds:create_gold(24,40)
	golds:create_gold(24,64)
	golds:create_gold(32,40)
	golds:create_gold(16,64)
	golds:create_gold(16,40)
	golds:create_gold(8,40)
	golds:create_gold(0,72)
	golds:create_gold(0,48)
	golds:create_gold(0,56)
	golds:create_gold(0,64)
	golds:create_gold(96,88)
	golds:create_gold(88,88)
	golds:create_gold(80,88)
	golds:create_gold(104,88)
	golds:create_gold(112,88)
	
end

function build_level()

end

function animate(me) 
		me.tick = (me.tick+1)%me.step
		if(me.tick == 0) then
			me.frame = me.frame%#me.sprites+1
		end
end

function sprite_collides(s1,s2)
	--currently requiring dimension field
	--further implementation with w/h?
	if (s1.x+s1.dimension<s2.x or s2.x+s2.dimension<s1.x or s1.y+s1.dimension<s2.y or s2.y+s2.dimension<s1.y) then
  return false
	else
 	return true
	end
end
function _draw()
	--draw layer 1
	blocks:draw()
	--draw layer 2
	player:draw()
	--draw layer 3
	golds:draw()
	--draw layer 4
	ui:draw()
end

function _update()
	blocks:update()
	player:update()
	golds:update()
	ui:update()
end

__gfx__
7bbbb3777bbbb3777777777700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
bb444447bb4444477bb3337700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
4fff0f774fff0f77bb44444700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
74ffff7774ffff774fff0f7700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
77fff77777fff77774ffff7700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
76111677761116777611167700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
7f555f777f555f77f75557f700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
77070777770707777707077700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
7777bb777bbbb3777777bb777bbbb377000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
7bbb3347bb4444477bbb3347bb444447000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
bb4444774fff0f77bb4444774fff0f77000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
4fff0f7774ffff774fff0f7774ffff77000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
74ffff7777fff77774ffff7777fff777000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
77116f77771617777f61177777161777000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
7f555777775f577777555f77775f5777000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
70777077770707777077707777070777000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
7bbb33777bbb33770000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
b4444447bbbbb3370000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
44444477444444470000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
4fff0f774fff0f770000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
74ffff7774ffff770000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
77116f7777116f770000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
775557777f5507770000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
77707077770777770000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000aa000000a7000000aa000000aa000000aa00000077000000a7000000aa000000aa000000aa000000000000000000000000000000000000000000000000000
00aaa90000aaa90000aaa90000aaa90000aaa900007aa90000a7770000aaa70000aaa90000aaa900000000000000000000000000000000000000000000000000
0aaaaa900aaaaa9007aaaa900aaaaa900aaaaa9007aaaa900a777a900aaa77700aaaaa900aaaaa90000000000000000000000000000000000000000000000000
0aaaa9900aaaa9900aaaa9900aaaa9900aaaa99007aaa9900777a9900aa777900aaaa9700aaaa990000000000000000000000000000000000000000000000000
00aa990000aa990000aa990000aa970000aa990000aa9900007a99000077790000aa970000aa9900000000000000000000000000000000000000000000000000
00099000000990000009900000099000000990000009900000099000000790000009700000099000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
77777777777000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
77766666677000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
77666666577000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
76555555577000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
75577777777000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
77777777777000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__sfx__
000100003f35027350273502735024350273502235027350223501f350273501d3501d3501b3502b3501b3501b3501835016350163501335013350113500f3500f35011350113501335013350163501735015350
